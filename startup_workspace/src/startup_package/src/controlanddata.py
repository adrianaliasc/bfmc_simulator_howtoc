import roslib
import sys
import rospy
import cv2
import numpy as np
import io
import time
import math
from enum import Enum
from std_msgs.msg import Float32MultiArray


from bfmclib.gps_s import Gps
from bfmclib.bno055_s import BNO055
from bfmclib.controller_p import Controller
from bfmclib.trafficlight_s import TLColor, TLLabel, TrafficLight

from threading import Thread
from templates.threadwithstop import ThreadWithStop
from bfmclib.camera_s import CameraHandler

class Turns(Enum):
    RIGHT = 0
    LEFT = 1
    STRAIGHT = 2
    ZEBRA = 3

class ControlAndDataThread(ThreadWithStop):

    def __init__(self, inPs, outPs):
        """
        This thread will collect the data necessary for the control of the car
        and act on the car as needed.
        :param outPs: list() - output pipelines
        :param inPs: list() - input pipelines
        """
        super(ControlAndDataThread, self).__init__()
        self.daemon = True
        self.inPs = inPs
        self.outPs = outPs

        # ======================== GAZEBO ELEMENTS ======================#
        self.car = Controller()
        print("Controller loaded")

        self.sem = TrafficLight()
        print("Traffic lights listener")

        self.gps = Gps()
        print("Gps loaded")

        self.bno = BNO055()
        print("BNO055 loaded")
        # ======================== GAZEBO ELEMENTS OVER =============== #
        # =================== DEBUGGING MANUAL DRIVING ELEMENTS ======= #
        self.speed = 0.0
        self.steer = 0.0
        self.manual_mode = True
        # ====================== DEBUGGING OVER ======================== #
        # ==================== CONTROLLER VARIABLES ==================== #
        self.steer_Kp = -335.4
        self.steer_Kd = 653.2 #294.2
        # ================= CONTROLLER VARIABLES OVER ================== #
        # ============= VALUES FOR INTERSECTION CROSSINGS ============== #
        self.duration_intersection = 6
        self.ongoing_intersection = False
        self.turns_count = -1 # Gets ONE added as an intersection maneuver added
        self.turns_left = [Turns.RIGHT, Turns.STRAIGHT, Turns.LEFT, Turns.LEFT, Turns.RIGHT, Turns.LEFT, Turns.LEFT, Turns.STRAIGHT, Turns.ZEBRA, Turns.STRAIGHT]
        self.index_intersection = 1
        self.yaw_per_step = []
        # ========== VALUES FOR INTERSECTION CROSSINGS OVER =========== #
        # ======================= TIMING ELEMENTS ===================== #
        # Would use process_time() if I could. :()
        self.curr_time = time.time()
        self.prev_time = time.time() - self.duration_intersection
        self.elapsed_time = self.curr_time-self.prev_time
        # ======================= TIMING ELEMENTS OVER ================ #
        # ======================= ROSPY SUBSCRIBER  =================== #
        rospy.Subscriber("sign_topic", Float32MultiArray, self.callback)
        # ======================= ROSPY SUBSCRIBER  OVER ============== #


    def debug_manual_drive(self, key):
        if key == ord('w'):
                self.speed = self.speed + 0.1
        if key == ord('a'):
                if(self.steer > -25):
                    self.steer = self.steer - 2.5
        if key == ord('s'):
                self.speed = self.speed - 0.1
        if key == ord('d'):
                if(self.steer < 25):
                    self.steer = self.steer + 2.5
        if key == ord('e'):
                self.steer = 0.0
                self.speed = 0.0
        self.car.drive(self.speed, self.steer)
        #print(self.speed)

    def choose_turn(self):
        turn = self.turns_left[self.turns_count]
        seconds = [0, 2.5, 6]

        if self.turns_left[self.turns_count] == Turns.RIGHT:
            self.maneuver(seconds, [0.35, 0.3], [0.0, 20], self.yaw_per_step, 0.12)
        elif self.turns_left[self.turns_count] == Turns.LEFT:
            self.maneuver(seconds, [0.7, 0.35], [0.0, -20], self.yaw_per_step, 0.12)
        elif self.turns_left[self.turns_count] == Turns.STRAIGHT:
            self.maneuver(seconds, [0.4, 0.5], [0.0, 0.0], self.yaw_per_step, 0.7)
        elif self.turns_left[self.turns_count] == Turns.ZEBRA:
            self.maneuver(seconds, [0.25, 0.25], [0.0, 0.0], self.yaw_per_step, 0.7)


    def maneuver(self, seconds, speeds, steers, yaw, yaw_P):
        yaw_current = self.bno.getYaw()*180/math.pi+180
        yaw_diff = yaw_current - yaw[self.index_intersection - 2]
        print "Yaw_diff: ", yaw_diff
        if(self.index_intersection < len(seconds) and self.elapsed_time < seconds[self.index_intersection] and self.elapsed_time > seconds[self.index_intersection-1]):
            self.index_intersection = self.index_intersection + 1
        #print "Self.steer: ", steers[self.index_intersection - 2]+0.25*yaw_diff
        self.car.drive(speeds[self.index_intersection - 2], steers[self.index_intersection - 2]+yaw_P*yaw_diff)

    def callback(self, data):
        print(data.data)

    def run(self):
        manual_mode = True
        slope_avg = 0;
        while self._running:
            self.curr_time = time.time()
            self.elapsed_time = self.curr_time-self.prev_time
            # Keys, for manual control
            key = self.inPs[0].recv()
            # Slope, for lane keeping
            lane_data = self.inPs[1].recv()
            slope_avg_prev = slope_avg
            slope_avg = lane_data[0]
            slope_diff = slope_avg - slope_avg_prev
            # Horz, for horizontal line detection
            horz_line = lane_data[1]

            if key == ord('x'):
                self.speed = 0.0
                self.steer = 0.0
                self.manual_mode = not self.manual_mode

            if(self.manual_mode):
                #print "Yaw: ", self.bno.getYaw()*180/math.pi+180
                print "Current time: ", self.curr_time
                print "Elapsed time: ", self.elapsed_time
                print "Yaw: ", self.bno.getYaw()*180/math.pi+180
                print(self.steer)
                self.debug_manual_drive(key)
            else:
                if(self.elapsed_time > self.duration_intersection):
                    if not horz_line:
                        self.steer = self.steer_Kp*slope_avg + self.steer_Kd*slope_diff
                        # print "Proportional result: ", self.steer_Kp*slope_avg
                        # print "Derivative result: ", self.steer_Kd*slope_diff
                        # print "Steering result: ", self.steer
                        self.car.drive(0.35, self.steer)
                    else:
                        self.turns_count = self.turns_count + 1
                        yaw_degrees = self.bno.getYaw()*180/math.pi+180
                        yaw_90 = int(round(yaw_degrees/90))*90
                        if self.turns_left[self.turns_count] == Turns.RIGHT:
                            self.yaw_per_step = [yaw_90, yaw_90-90]
                        elif self.turns_left[self.turns_count] == Turns.LEFT:
                            self.yaw_per_step = [yaw_90, yaw_90+90]
                        else:
                            self.yaw_per_step = [yaw_90, yaw_90]
                        self.index_intersection = 1
                        self.prev_time = self.curr_time
                else:
                    self.choose_turn()

            for outP in self.outPs:
                    outP.send([[stamp], data])
