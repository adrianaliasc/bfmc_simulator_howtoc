#!/usr/bin/python

import sys
sys.path.append('.')

import time
import signal
import rospy
import cv2
from multiprocessing import Pipe, Process, Event
import threading

from src.hardware.camera.cameraPublisher            import CameraPublisherThread
from src.imageprocessing.signcascadeprocess         import SignCascadeProcess
from src.imageprocessing.signsendthread         import SignSendThread
from controlanddata                     import ControlAndDataThread

rospy.init_node('sign_node', anonymous=True)

allProcesses = list()
allMainThreads = list()

# ========== SETTING THREADS AND PROCESSES =============
camToSignR = []
camToSignS = []
signToSendR = []
signToSendS = []
for i in range(4):
    # Camera to Sign
    camToSignR_instance, camToSignS_instance = Pipe(duplex=False)
    camToSignR.append(camToSignR_instance)
    camToSignS.append(camToSignS_instance)
    # Sign Detectors to Gatherer
    signToSendR_instance, signToSendS_instance = Pipe(duplex=False)
    signToSendR.append(signToSendR_instance)
    signToSendS.append(signToSendS_instance)

camThr = CameraPublisherThread([camToSignS[0], camToSignS[1], camToSignS[2], camToSignS[3]])
allMainThreads.append(camThr)

signProc_1 = SignCascadeProcess([camToSignR[0]], [signToSendS[0]], "stopClassifier.xml", "STOP")
signProc_2 = SignCascadeProcess([camToSignR[1]], [signToSendS[1]], "pedestrianClassifier.xml", "CROSSWALK")
signProc_3 = SignCascadeProcess([camToSignR[2]], [signToSendS[2]], "priorityClassifier.xml", "PRIORITY")
signProc_4 = SignCascadeProcess([camToSignR[3]], [signToSendS[3]], "parkingClassifier.xml", "PARKING")
allProcesses.append(signProc_1)
allProcesses.append(signProc_2)
allProcesses.append(signProc_3)
allProcesses.append(signProc_4)

signSendThr = SignSendThread([signToSendR[0], signToSendR[1], signToSendR[2], signToSendR[3]],[])
allMainThreads.append(signSendThr)
# ============ PIPE SETTINGS OVER (THREADS AND PROCESSES) ==============

print("Starting main threads!", allMainThreads)
for thr in allMainThreads:
    thr.daemon = True
    thr.start()

print("Starting the processes!",allProcesses)
for proc in allProcesses:
    proc.daemon = True
    proc.start()

while not rospy.is_shutdown():
    pass

print("\nActie threads: ", threading.active_count())
print("\nCatching a KeyboardInterruption exception! Shutdown all processes.\n")
for proc in allProcesses:
    if hasattr(proc,'stop') and callable(getattr(proc,'stop')):
        print("Process with stop",proc)
        proc.stop()
        proc.join()
    else:
        print("Process without stop",proc)
        proc.terminate()
        proc.join()

print("\nCatching a KeyboardInterruption exception! Shutdown all main threads.\n")
for thr in allMainThreads:
    if hasattr(thr,'stop') and callable(getattr(thr,'stop')):
        # Sadly waiting for them to join freezes the code, so this is a quick awful dirty fix.
        print("Thread with stop",thr)
        thr.stop()
    else:
        print("Thread without stop",thr)
        thr.terminate()
