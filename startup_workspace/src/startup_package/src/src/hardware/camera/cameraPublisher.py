import roslib
import sys
import rospy
import cv2
import numpy as np
import io
import time

from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


from threading import Thread
from multiprocessing import Process
from templates.threadwithstop import ThreadWithStop
from bfmclib.camera_s import CameraHandler

class CameraPublisherThread(ThreadWithStop):

    def __init__(self, outPs):
        """
        This thread will send camera images.
        Note: We use a ThreadWithStop to which we add an output pipe, we have another
        template thread, WorkerThread, which by default has input and output pipes,
        but these are not difficult to add.
        :param outPs: list() - output pipelines
        """

        super(CameraPublisherThread, self).__init__()
        self.daemon = True
        self.outPs      = outPs
        # ===================== GAZEBO ELEMENT ======================== #
        self.cam = CameraHandler()
        # ===================== GAZEBO ELEMENT DONE =================== #
        # ===================== RECORDING STUFF ======================= #
        self.index = 2
        self.enable_record = False
        self.fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        self.out_record = cv2.VideoWriter(('map_automated'+str(self.index)+'.avi'),self.fourcc, 20.0, (640,480))
        # ===================== RECORDING STUFF DONE ================== #
        # ===================== FILTERING STUFF ======================= #
        self.filtered = False
        # ===================== FILTERING STUFF DONE ================== #

    def run(self):
        # Filter the BLACK frames
        while not self.filtered:
            data = self.cam.getImage()
            #print data
            if (len(data.shape) == 3):
                self.filtered = True

        while self._running:
            # Actually use the data
            data = self.cam.getImage()

            if self.enable_record:
                self.out_record.write(data)

            data_grey = cv2.cvtColor(data, cv2.COLOR_BGR2GRAY)

            for outP in self.outPs:
                outP.send(data_grey)
