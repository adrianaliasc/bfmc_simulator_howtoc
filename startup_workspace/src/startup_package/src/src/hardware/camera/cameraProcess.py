import multiprocessing
from multiprocessing import Process

from templates.workerprocess import WorkerProcess
from src.hardware.camera.cameraPublisher import CameraPublisher

class CameraProcess(WorkerProcess):

    def __init__(self, inPs, outPs, daemon = True):
        """

        :param inPs: list() - input pipes - empty
        :param outPs: list() - output pipes
        :param daemon: bool, optional, make it True
        """

        super(CameraProcess, self).__init__(inPs, outPs, daemon=True)


    def _init_threads(self):

        camTh = CameraPublisher(self.outPs)
        self.threads.append(camTh)
