import cv2
import numpy as np

"""
            Function for detecting lines
    Structure:
 - prepare_image()
    - read image and process it: 
        - convert to gray, other preparations that may be necessary (DEPENDING ON THE FINAL IMPLEMENTATION)
        - apply canny
    * returns: img 
 
 - make_masked()
    - apply a 'mask' (through a filled polygon) to obtain the region of interest (ROI)
    * returns: img
 
 - get_lines()
    - apply HoughLinesPoly() on the image to get the best lines of the image
    * returns: line as [X_start, Y_start, X_end, Y_end] (4 variables)
    
 - average_slope_intercept()
    - find the left and right lane lines by averaging all the other
    - here the horizontal lines can also be computed
        - uses make_points() to convert the line from slope and angle to initial and final point coordinates
        * returns: 4 variables: [x1 y1, x2 y2]
    * returns: 3 lines: left, right, horizontal as [x1 y1, x2 y2] x 3 (from make_points) or np.zeros(4) arrays
 
 - get_middle_line()
    - finds the 'middle' line that is going to be used for control
    
##########################################################################

 * FOR TESTING AND DEBUGGING:
 - display_lines()
    - writes all the lines on the recorded image and displays it
"""

##########################################################################
#                      GLOBAL VARIABLES DECLARATION                      #

# path = 'D:/Old Laptop Files/Projects/Python/MachineLearning/LineDetection/TestVideos/'
path = 'D:/Projects/Media/LaneDetection/Videos/Simulator1/'
video_name = 'output04.avi'

load_video = path + video_name
print(load_video)

# Image dimensions: !!!!!!!!MODIFY ACCORDING TO THE VIDEO!!!!!!!!
width = 640
height = 480

# Size and shape of the mask:
#                       bl          tl          tr          br
# polygon = np.array([[0, 1043], [468, 370], [893, 370], [1643, 1043]])
polygon = np.array([[0, 440], [175, 200], [450, 200], [640, 440]])

# Zeros matrix the size of the image to be analysed:
stencil = np.zeros((height, width))

# Filled stencil
cv2.fillConvexPoly(stencil, polygon, 255)

# Store 'n_lines' previous lanes
n_lines = 5
previous_lines_left = np.zeros((n_lines, 4))
previous_lines_right = np.zeros((n_lines, 4))
previous_lines_horizontal = np.zeros((n_lines, 4))

#       ONLY FOR DEBUG
# Make full screen
cv2.namedWindow('Result', cv2.WND_PROP_FULLSCREEN)
cv2.setWindowProperty('Result', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)


####################################################################


def prepare_image(image):
    img_blurred = cv2.GaussianBlur(image, (7, 7), 1)
    gray_image = cv2.cvtColor(img_blurred, cv2.COLOR_BGR2GRAY)
    canny_image = cv2.Canny(gray_image, 50, 150, 3, None, True)
    # arguments: 2=minValue, 3=maxValue (both for thresholding),
    # 4=Sobel kernel size (default 3),
    # 5=true/false - specifies the method to be used (True is better)
    return canny_image


def make_masked(image):
    # for whatever reason the global stencil does not work
    _stencil = np.zeros_like(image)
    cv2.fillConvexPoly(_stencil, polygon, 255)
    masked = cv2.bitwise_and(image, image, mask=_stencil)
    return masked


def get_lines(image):
    # Parameters:
    # output - vector that stores (Xstart, Ystart, Xend, Yend)
    # 1 - image: grayscale (binary, actually)
    # 2 - rho
    # 3 - theta
    # 4 - threshold
    # ...
    _lines = cv2.HoughLinesP(image, 2.5, np.pi / 180, 60, np.array([]), minLineLength=15, maxLineGap=10)
    if _lines is not None:
        return _lines
    else:
        return np.array([[[np.nan, np.nan, np.nan, np.nan]]])


def make_points(line):
    global height
    slope, intercept = line
    if abs(slope) <= 0.01:
        slope = 0.01
    y1 = int(height - 30)
    y2 = int((y1 * 3 / 5))  # !!!!!!!!!! CHANGE ACCORDING TO THE TESTS ON THE ACTUAL VIDEO !!!!!!!!!
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    return np.array([x1, y1, x2, y2])


def average_slope_intercept(input_lines):
    left_fit = []
    right_fit = []
    horizontal_fit = []
    if not np.isnan(np.sum(input_lines)):
        for line in input_lines:
            for x1, y1, x2, y2 in line:
                if round(x1) == round(x2):
                    continue
                if abs(y1 - y2) > 5:
                    fit = np.polyfit((int(x1), int(x2)), (int(y1), int(y2)), 1)
                    slope = fit[0]
                    intercept = fit[1]
                    if slope < 0:
                        left_fit.append((slope, intercept))
                    else:
                        right_fit.append((slope, intercept))
                else:
                    fit = np.polyfit((int(x1), int(x2)), (int(y1), int(y2)), 1)
                    horizontal_fit.append((fit[0], fit[1]))

    _left_line = np.zeros(4, 'int32')
    _right_line = np.zeros(4, 'int32')
    _horizontal_line = np.zeros(4, 'int32')
    if left_fit:
        left_fit_average = np.average(left_fit, axis=0)
        _left_line = make_points(left_fit_average)
    if right_fit:
        right_fit_average = np.average(right_fit, axis=0)
        _right_line = make_points(right_fit_average)
    if horizontal_fit:
        horizontal_fit_average = np.average(horizontal_fit, axis=0)
        _horizontal_line = make_points(horizontal_fit_average)

    lines_result = np.array([[_left_line, _right_line, _horizontal_line]])
    return lines_result


def get_middle_line(left, right):
    global width

    xl1 = left[0, 0]
    yl1 = left[0, 1]
    xl2 = left[0, 2]
    yl2 = left[0, 3]

    xr1 = right[0, 0]
    yr1 = right[0, 1]
    xr2 = right[0, 2]
    yr2 = right[0, 3]

    x1_avg = ((xl1 + xr1) / 2)
    x2_avg = ((xl2 + xr2) / 2)
    y1_avg = ((yl1 + yr1) / 2)
    y2_avg = ((yl2 + yr2) / 2)

    y_top = 300
    y_bot = 640
    x_bot = width / 2

    if abs(x1_avg - x2_avg) < 5:
        x_top = width / 2
        m = 0
    else:
        m = (y1_avg - y2_avg) / (x1_avg - x2_avg)
        x_top = x_bot - ((y_bot - y_top) / m)
    print(m)
    print(x1_avg, x2_avg, y1_avg, y2_avg)
    return m, np.array([x_bot, y_bot, x_top, y_top])


def display_lines(image, _all_lines):
    image_aux = np.zeros_like(image)
    line_count = 0
    for line in _all_lines:
        line_count += 1
        if line.all() != np.array([0, 0, 0, 0]).all():
            x1, y1, x2, y2 = line.reshape(4)
            if line_count == 1 or line_count == 2:
                color = (int(255), int(0), int(0))
            elif line_count == 3:
                color = (int(0), int(255), int(0))
            elif line_count == 4:
                color = (int(0), int(0), int(255))
            else:
                color = (int(255), int(255), int(0))

            cv2.line(image_aux, (int(x1), int(y1)), (int(x2), int(y2)), color, 5)
    cv2.line(image_aux, (0, 500), (1640, 500), (int(255), int(255), int(255)), 5)
    return cv2.addWeighted(image, 0.5, image_aux, 1, 1)


test = 1
frame_count = 0
font = cv2.FONT_HERSHEY_SIMPLEX


def stack_average_lines(line):
    global previous_lines_left, previous_lines_right, previous_lines_horizontal
    previous_lines_horizontal = np.vstack((previous_lines_horizontal, np.array([line[0, 2]])))
    previous_lines_horizontal = np.delete(previous_lines_horizontal, 0, 0)
    averaged_lines_horizontal = np.average(previous_lines_horizontal, axis=0)

    previous_lines_left = np.vstack((previous_lines_left, np.array([line[0, 0]])))
    previous_lines_left = np.delete(previous_lines_left, 0, 0)
    averaged_lines_left = np.average(previous_lines_left, axis=0)

    previous_lines_right = np.vstack((previous_lines_right, np.array([line[0, 1]])))
    previous_lines_right = np.delete(previous_lines_right, 0, 0)
    averaged_lines_right = np.average(previous_lines_right, axis=0)

    return averaged_lines_left, averaged_lines_right, averaged_lines_horizontal


def display_frame_nr(counter):
    counter = counter + 1
    print(counter)
    return counter


if __name__ == '__main__':
    cap = cv2.VideoCapture(load_video)
    while cap.isOpened():
        frame_count = display_frame_nr(frame_count)
        _, img = cap.read()

        if frame_count == 7701:
            cv2.imwrite("Frame_7701.png", img)

        img_aux = np.copy(img)
        img_aux = prepare_image(img_aux)
        img_aux = make_masked(img_aux)

        lines = get_lines(img_aux)
        averaged_lines = average_slope_intercept(lines)

        left_line, right_line, horizontal_line = stack_average_lines(averaged_lines)

        slope_c, middle_line = get_middle_line(np.array([left_line]), np.array([right_line]))

        all_lines = np.array([averaged_lines[0, 0], averaged_lines[0, 1], averaged_lines[0, 2], middle_line])

        if all_lines is not None:
            result = display_lines(img, all_lines)
            cv2.imshow('Result', result)
        else:
            cv2.imshow('Result', img)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

'''
    'slope_c' should be used for control !!!!!!!!!!! 
'''
