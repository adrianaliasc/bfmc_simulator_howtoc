import multiprocessing
from multiprocessing import Process

from templates.workerprocess import WorkerProcess
from src.imageprocessing.signcascadethread import SignCascadeThread

class SignCascadeProcess(WorkerProcess):

    def __init__(self, inPs, outPs, cascade_name, window_name,daemon = True):
        """

        :param inPs: list() - input pipes - from CameraPublisher
        :param outPs: list() - output pipes - Nothing yet
        :param daemon: bool, optional, make it True
        :param cascade_name: the name of the cascade that will be loaded up
        """
        self.cascade_name = cascade_name
        self.window_name = window_name
        super(SignCascadeProcess, self).__init__(inPs, outPs, daemon=True)


    def _init_threads(self):

        signCascThr = SignCascadeThread(self.inPs, self.outPs, self.cascade_name, self.window_name)
        self.threads.append(signCascThr)
