import cv2
import numpy as np
import time
import math
import joblib

from threading import Thread
from multiprocessing import Process
from templates.threadwithstop import ThreadWithStop
# For manual control
from bfmclib.controller_p import Controller



############################################################################
#                      GLOBAL VARIABLES DECLARATION                        #
#                      TURNED TO CLASSES VARIABLES                         #
class SignCascadeThread(ThreadWithStop):

    def __init__(self, inPs, outPs, cascade_name, window_name):

        super(SignCascadeThread, self).__init__()
        # Image dimensions:
        self.height = 480
        self.width = 640
        # Thread variables:
        self.daemon = True
        self.outPs = outPs
        self.inPs = inPs
        self.cascade_name = cascade_name
        self.window_name = window_name
        # Mask variables:
        self.polygon = np.array([[440, 220], [440, 0], [640, 0], [640, 220]])
        self.stencil = np.zeros((self.height, self.width))
        # Cascade variables:
        self.percentage = 0.0

        # Load cascade:
        self.cascade = cv2.CascadeClassifier()
        if not self.cascade.load("Classifiers/" + self.cascade_name):
            print("Error loading " + self.cascade_name + " !")
            exit(0)

    def draw_image(self, img, sign):
        # Draw on image:
        if (self.percentage > 10):
            for (x, y, w, h) in sign:
                center_ellipse = ((x + w // 2), (y + h // 2))
                center_text = ((x + w // 2)-30, (y + h // 2)+50)
                font = cv2.FONT_HERSHEY_SIMPLEX
                fontScale = 0.75
                color = (255, 0, 0)
                img = cv2.ellipse(img, center_ellipse, (w // 2, h // 2), 0, 0, 360, (255, 0, 255), 4)
                #cv2.putText(img, self.cascade_name, center, font, fontScale, color)
                cv2.putText(img, str(self.percentage) + "%", center_text, font, fontScale, color)



    def run(self):
        displacement_y = self.height/2
        displacement_x = self.width/2;

        positives = []
        while len(positives) < 20:
            positives.append(0)

        while self._running:
            img = self.inPs[0].recv()

            img_roi = img[0:displacement_y, displacement_x:self.width]
            sign = self.cascade.detectMultiScale(img_roi)
            positives.pop(0)
            positives.append(len(sign) != 0)
            self.percentage = float(sum(positives))/len(positives)*100
            self.draw_image(img_roi, sign)
            cv2.imshow(self.window_name, img_roi)

            for outP in self.outPs:
                outP.send(self.percentage)

            key = cv2.waitKey(1)





'''
    'slope_c' should be used for control !!!!!!!!!!!
'''
