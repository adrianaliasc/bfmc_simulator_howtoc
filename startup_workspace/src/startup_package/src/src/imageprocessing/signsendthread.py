import cv2
import numpy as np
import time
import math
import joblib
import rospy
from std_msgs.msg import Float32MultiArray

from threading import Thread
from multiprocessing import Process
from templates.threadwithstop import ThreadWithStop
# For manual control
from bfmclib.controller_p import Controller



############################################################################
#                      GLOBAL VARIABLES DECLARATION                        #
#                      TURNED TO CLASSES VARIABLES                         #
class SignSendThread(ThreadWithStop):

    def __init__(self, inPs, outPs):

        super(SignSendThread, self).__init__()
        # Thread variables:
        self.daemon = True
        self.outPs = outPs
        self.inPs = inPs
        # Publisher variables:
        self.pub = rospy.Publisher("sign_topic", Float32MultiArray, queue_size=5)


    def run(self):

        percentages = []
        for i in range(len(self.inPs)):
            percentages.append(0)

        while self._running:
            img = self.inPs[0].recv()
            for i in range(len(self.inPs)):
                percentages[i] = self.inPs[i].recv()

            percentages_out = Float32MultiArray()
            percentages_out.data = percentages;
            self.pub.publish(percentages_out)
            print(percentages)
