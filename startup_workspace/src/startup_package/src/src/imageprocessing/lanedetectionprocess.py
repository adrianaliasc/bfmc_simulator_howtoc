import multiprocessing
from multiprocessing import Process

from templates.workerprocess import WorkerProcess
from src.imageprocessing.lanedetectionthread import LaneDetectionThread

class LaneDetectionProcess(WorkerProcess):

    def __init__(self, inPs, outPs, daemon = True):
        """

        :param inPs: list() - input pipes - from CameraPublisher
        :param outPs: list() - output pipes - Nothing yet
        :param daemon: bool, optional, make it True
        """

        super(LaneDetectionProcess, self).__init__(inPs, outPs, daemon=True)


    def _init_threads(self):

        laneDetTh = LaneDetectionThread(self.inPs, self.outPs)
        self.threads.append(laneDetTh)
