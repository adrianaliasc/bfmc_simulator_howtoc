import cv2
import numpy as np
import time
import math

"""
            Function for detecting lines
    Structure:
 - prepare_image()
    - read image and process it:
        - convert to gray, other preparations that may be necessary (DEPENDING ON THE FINAL IMPLEMENTATION)
        - apply canny
    * returns: img

 - make_masked()
    - apply a 'mask' (through a filled polygon) to obtain the region of interest (ROI)
    * returns: img

 - get_lines()
    - apply HoughLinesPoly() on the image to get the best lines of the image
    * returns: line as [X_start, Y_start, X_end, Y_end] (4 variables)

 - average_slope_intercept()
    - find the left and right lane lines by averaging all the other
    - here the horizontal lines can also be computed
        - uses make_points() to convert the line from slope and angle to initial and final point coordinates
        * returns: 4 variables: [x1 y1, x2 y2]
    * returns: 3 lines: left, right, horizontal as [x1 y1, x2 y2] x 3 (from make_points) or np.zeros(4) arrays

 - get_middle_line()
    - finds the 'middle' line that is going to be used for control

##########################################################################

 * FOR TESTING AND DEBUGGING:
 - display_lines()
    - writes all the lines on the recorded image and displays it
"""

from threading import Thread
from multiprocessing import Process
from templates.threadwithstop import ThreadWithStop
# For manual control
from bfmclib.controller_p import Controller



##########################################################################
#                      GLOBAL VARIABLES DECLARATION                      #
#                      TURNED TO CLASSES VARIABLES                       # xoxo
class LaneDetectionThread(ThreadWithStop):

    def __init__(self, inPs, outPs):

        super(LaneDetectionThread, self).__init__()

        # self.path = 'D:/Projects/Python/MachineLearning/LineDetection/TestVideos/'
        # self.video_name = 'city_road-checkpiont.h264'

        # load_video = path + video_name

        # Image dimensions: !!!!!!!!MODIFY ACCORDING TO THE VIDEO!!!!!!!!
        self.height = 480
        self.width = 640

        # Size and shape of the mask:
        mth = 280 # mask top height
        mbh = 460 # mask bottom height
        #                          bl          tl          tr          br
        self.polygon = np.array([[0, mbh], [110, mth], [500, mth], [640, mbh]])
        # Zeros matrix the size of the image to be analysed:
        self.stencil = np.zeros((self.height, self.width))
        # Filled stencil
        cv2.fillConvexPoly(self.stencil, self.polygon, 255)

        # Store 'n_lines' previous lanes
        self.n_lines = 3
        self.previous_lines_left = np.zeros((self.n_lines, 4))
        self.previous_lines_right = np.zeros((self.n_lines, 4))
        # self.previous_lines_horizontal = np.zeros((self.n_lines, 4))

        #       ONLY FOR DEBUG
        # Make full screen
        # cv2.namedWindow('Result', cv2.WND_PROP_FULLSCREEN)
        # cv2.setWindowProperty('Result', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

        self.daemon = True
        self.outPs = outPs
        self.inPs = inPs


    ####################################################################

    def prepare_image(self, image):
        img_blurred = cv2.GaussianBlur(image, (7, 7), 1)
        gray_image = img_blurred #cv2.cvtColor(img_blurred, cv2.COLOR_BGR2GRAY)
        canny_image = cv2.Canny(gray_image, 100, 150, apertureSize=3, L2gradient=False)
        # arguments: 2=minValue, 3=maxValue (both for thresholding),
        # 4=Sobel kernel size (default 3),
        # 5=true/false - specifies the method to be used (True is better)
        return canny_image

    def make_masked(self, image):
        # for whatever reason the global stencil does not work
        _stencil = np.zeros_like(image)
        cv2.fillConvexPoly(_stencil, self.polygon, 255)
        #cv2.imshow("convexPoly", cv2.fillConvexPoly(_stencil, self.polygon, 255))
        masked = cv2.bitwise_and(image, image, mask=_stencil)
        #cv2.imshow("mask", masked)
        return masked

    def get_lines(self, image):
        # Parameters:
        # output - vector that stores (Xstart, Ystart, Xend, Yend)
        # 1 - image: grayscale (binary, actually)
        # 2 - rho
        # 3 - theta
        # 4 - threshold
        # ...
        _lines = cv2.HoughLinesP(image, 2.5, np.pi / 180, 60, np.array([]), minLineLength=3, maxLineGap=18)
        if _lines is not None:
            return _lines
        else:
            return np.array([[[np.nan, np.nan, np.nan, np.nan]]])

    def make_points(self, line):
        slope, intercept = line
        if abs(slope) <= 0.01:
            slope = 0.01
        y1 = self.height - 30
        y2 = (y1 * 3 / 5)  # !!!!!!!!!! CHANGE ACCORDING TO THE TESTS ON THE ACTUAL VIDEO !!!!!!!!!
        x1 = (y1 - intercept) / slope
        x2 = (y2 - intercept) / slope
        return np.array([int(x1), int(y1), int(x2), int(y2)])

    def average_slope_intercept(self, input_lines):
        left_fit = []
        right_fit = []
        # horizontal_fit = []
        if not np.isnan(np.sum(input_lines)):
            for line in input_lines:
                for x1, y1, x2, y2 in line:
                    if x1 == x2:
                        continue
                    if abs(y1 - y2) > 25:
                        fit = np.polyfit((int(x1), int(x2)), (int(y1), int(y2)), 1)
                        slope = fit[0]
                        intercept = fit[1]
                        if slope < 0:
                            left_fit.append((slope, intercept))
                        else:
                            right_fit.append((slope, intercept))
                    # else:
                    # fit = np.polyfit((int(x1), int(x2)), (int(y1), int(y2)), 1)
                    # horizontal_fit.append((fit[0], fit[1]))

        _left_line = np.zeros(4, 'int32')
        _right_line = np.zeros(4, 'int32')
        # _horizontal_line = np.zeros(4, 'int32')
        if left_fit:
            left_fit_average = np.average(left_fit, axis=0)
            _left_line = self.make_points(left_fit_average)
        if right_fit:
            right_fit_average = np.average(right_fit, axis=0)
            _right_line = self.make_points(right_fit_average)
        # if horizontal_fit:
        # horizontal_fit_average = np.average(horizontal_fit, axis=0)
        # _horizontal_line = self.make_points(horizontal_fit_average)

        # lines_result = np.array([[_left_line, _right_line, _horizontal_line]])
        lines_result = np.array([[_left_line, _right_line]])
        return lines_result

    def get_middle_line(self, left, right):
        xl1 = left[0, 0]
        yl1 = left[0, 1]
        xl2 = left[0, 2]
        yl2 = left[0, 3]

        xr1 = right[0, 0]
        yr1 = right[0, 1]
        xr2 = right[0, 2]
        yr2 = right[0, 3]

        x1_avg = ((xl1 + xr1) / 2)
        x2_avg = ((xl2 + xr2) / 2)
        y1_avg = ((yl1 + yr1) / 2)
        y2_avg = ((yl2 + yr2) / 2)

        y_top = 300
        y_bot = 640
        x_bot = self.width / 2

        if abs(x1_avg - x2_avg) < 10:
            x_top = self.width / 2
            m = 0
        else:
            m = (y1_avg - y2_avg) / (x1_avg - x2_avg)
            x_top = x_bot - ((y_bot - y_top) / m)
        # print(m)
        # print(x1_avg, x2_avg, y1_avg, y2_avg)
        return m, np.array([x_bot, y_bot, x_top, y_top])

    def display_lines(self, image, _all_lines):
        image_aux = np.zeros_like(image)
        line_count = 0
        for line in _all_lines:
            line_count += 1
            if line.all() != np.array([0, 0, 0, 0]).all():
                x1, y1, x2, y2 = line.reshape(4)
                if line_count == 1 or line_count == 2:
                    color = (int(255), int(0), int(0))
                elif line_count == 3:
                    color = (int(255), int(0), int(0))
                else:
                    color = (int(255), int(0), int())

                cv2.line(image_aux, (int(x1), int(y1)), (int(x2), int(y2)), color, 5)
        # cv2.line(image_aux, (0, 500), (1640, 500), (int(255), int(255), int(255)), 5)
        return cv2.addWeighted(image, 0.5, image_aux, 1, 1)

    def stack_average_lines(self, line):
        # self.previous_lines_horizontal = np.vstack((self.previous_lines_horizontal, np.array([line[0, 2]])))
        # self.previous_lines_horizontal = np.delete(self.previous_lines_horizontal, 0, 0)
        # self.averaged_lines_horizontal = np.average(self.previous_lines_horizontal, axis=0)

        self.previous_lines_left = np.vstack((self.previous_lines_left, np.array([line[0, 0]])))
        self.previous_lines_left = np.delete(self.previous_lines_left, 0, 0)
        averaged_lines_left = np.average(self.previous_lines_left, axis=0)

        self.previous_lines_right = np.vstack((self.previous_lines_right, np.array([line[0, 1]])))
        self.previous_lines_right = np.delete(self.previous_lines_right, 0, 0)
        averaged_lines_right = np.average(self.previous_lines_right, axis=0)

        return averaged_lines_left, averaged_lines_right  # , self.averaged_lines_horizontal

    def display_frame_nr(self, counter):
        counter = counter + 1
        # print(counter)
        return counter

    def average(self, slopes):
        return sum(slopes)/len(slopes)

    def if_horizontal(self, local_img):
        y,x = local_img.shape
        local_img = local_img[y-120:y-000, x-510:x-100]
        #cv2.imshow("secondary", local_img)
        gray = local_img #cv2.cvtColor(local_img,cv2.COLOR_BGR2GRAY)
        # gray = cv2.bitwise_not(gray)
        bw = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C,  cv2.THRESH_BINARY, 15, -2)
        horizontal = np.copy(bw)
        cols = horizontal.shape[1]
        horizontal_size = cols // 7
        horizontalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontal_size, 5))
        horizontal = cv2.erode(horizontal, horizontalStructure)
        horizontal = cv2.dilate(horizontal, horizontalStructure)

        _, contours, hier = cv2.findContours(horizontal,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
        for cnt in contours:
            if 1500<cv2.contourArea(cnt)<3500:
                ##rect = cv2.minAreaRect(cnt)
                #box = cv2.boxPoints(rect)
                #box = np.int0(box)
                ##cv2.drawContours(local_img,[cnt],0,(0,255,0),2)
                #cv2.drawContours(horizontal,[cnt],0,255,-1)
                #cv2.putText(local_img, "HORIZONTAL LINE", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
                return True
        if not contours:
            #cv2.putText(local_img, " NO HORIZONTAL LINE", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
            return False

    def run(self):
        wait_count = 0

        slopes = []
        while len(slopes) < 10:
            slopes.append(0)

        while self._running:
            # Horz_line, for horizontal line detection
            horz_line = False
            # frame_count = self.display_frame_nr(frame_count)
            img = self.inPs[0].recv()
            img_aux = np.copy(img)
            img_aux = self.prepare_image(img_aux)
            img_aux = self.make_masked(img_aux)

            lines = self.get_lines(img_aux)
            averaged_lines = self.average_slope_intercept(lines)

            # left_line, right_line, horizontal_line = self.stack_average_lines(averaged_lines)
            left_line, right_line = self.stack_average_lines(averaged_lines)

            slope_c, middle_line = self.get_middle_line(np.array([left_line]), np.array([right_line]))
            slopes.pop(0)
            slope_control = 1/(np.arctan(slope_c)*180/math.pi)
            if slope_control == float('inf'):
                slope_control = 0
            slopes.append(slope_control)
            # all_lines = np.array([averaged_lines[0, 0], averaged_lines[0, 1], averaged_lines[0, 2], middle_line])
            all_lines = np.array([averaged_lines[0, 0], averaged_lines[0, 1], middle_line])

            horz_line = self.if_horizontal(img)
            if all_lines is not None:
                #result = self.display_lines(img, all_lines)
                #cv2.imshow('Result', result)
                cv2.imshow('Result', img)
            else:
                cv2.imshow('Result', img)

            key = cv2.waitKey(1)
            if key & 0xFF == ord('q'):
                break

            #print("Slope angles: ", slope_control)
            #print("Slope_c: ", slope_c)
            self.outPs[0].send(key)
            self.outPs[1].send([self.average(slopes), horz_line])

        cv2.destroyAllWindows()


'''
    'slope_c' should be used for control !!!!!!!!!!!
'''
