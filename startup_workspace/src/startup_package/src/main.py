#!/usr/bin/python

import sys
sys.path.append('.')

import time
import signal
import rospy
import cv2
from multiprocessing import Pipe, Process, Event
import threading

from src.hardware.camera.cameraPublisher            import CameraPublisherThread
from src.imageprocessing.lanedetectionprocess       import LaneDetectionProcess
from src.imageprocessing.signcascadeprocess         import SignCascadeProcess
from controlanddata                     import ControlAndDataThread
# For manual control!
from bfmclib.controller_p import Controller


rospy.init_node('main_node', anonymous=True)
enableCode = True

allProcesses = list()
allMainThreads = list()

# ========== SETTING THREADS AND PROCESSES =============
camToLaneR, camToLaneS = Pipe(duplex=False);

laneToControlR, laneToControlS = Pipe(duplex=False);
laneToControlSlopeR, laneToControlSlopeS = Pipe(duplex=False);
controlAndDataThr = ControlAndDataThread([laneToControlR, laneToControlSlopeR],[])
allMainThreads.append(controlAndDataThr)

camThr = CameraPublisherThread([camToLaneS])
allMainThreads.append(camThr)

laneProc = LaneDetectionProcess([camToLaneR], [laneToControlS, laneToControlSlopeS])
allProcesses.append(laneProc)
# ============ PIPE SETTINGS OVER (THREADS AND PROCESSES) ==============

print("Starting main threads!", allMainThreads)
for thr in allMainThreads:
    thr.daemon = True
    thr.start()

print("Starting the processes!",allProcesses)
for proc in allProcesses:
    proc.daemon = True
    proc.start()

while not rospy.is_shutdown():
    pass

print("\nActie threads: ", threading.active_count())
print("\nCatching a KeyboardInterruption exception! Shutdown all processes.\n")
for proc in allProcesses:
    if hasattr(proc,'stop') and callable(getattr(proc,'stop')):
        print("Process with stop",proc)
        proc.stop()
        proc.join()
    else:
        print("Process without stop",proc)
        proc.terminate()
        proc.join()

print("\nCatching a KeyboardInterruption exception! Shutdown all main threads.\n")
for thr in allMainThreads:
    if hasattr(thr,'stop') and callable(getattr(thr,'stop')):
        # Sadly waiting for them to join freezes the code, so this is a quick awful dirty fix.
        print("Thread with stop",thr)
        thr.stop()
    else:
        print("Thread without stop",thr)
        thr.terminate()
