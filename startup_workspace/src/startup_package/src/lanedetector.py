import cv2
import numpy as np

"""
            Function for detecting lines
    Structure:
 - prepare_image()
    - read image and process it: 
        - convert to gray, other preparations that may be necessary (DEPENDING ON THE FINAL IMPLEMENTATION)
        - apply canny
    * returns: img 

 - make_masked()
    - apply a 'mask' (through a filled polygon) to obtain the region of interest (ROI)
    * returns: img

 - get_lines()
    - apply HoughLinesPoly() on the image to get the best lines of the image
    * returns: line as [X_start, Y_start, X_end, Y_end] (4 variables)

 - average_slope_intercept()
    - find the left and right lane lines by averaging all the other
    - here the horizontal lines can also be computed
        - uses make_points() to convert the line from slope and angle to initial and final point coordinates
        * returns: 4 variables: [x1 y1, x2 y2]
    * returns: 3 lines: left, right, horizontal as [x1 y1, x2 y2] x 3 (from make_points) or np.zeros(4) arrays

 - get_middle_line()
    - finds the 'middle' line that is going to be used for control

##########################################################################

 * FOR TESTING AND DEBUGGING:
 - display_lines()
    - writes all the lines on the recorded image and displays it
"""

##########################################################################
#                      GLOBAL VARIABLES DECLARATION                      #


class LaneDetector():

    def __init__(self):


        self.path = 'D:/Projects/Media/LaneDetection/Videos/'
        self.video_name = 'city_road-checkpiont.h264'

        self.test = 1
        self.frame_count = 0
        self.font = cv2.FONT_HERSHEY_SIMPLEX

        self.load_video = self.path + self.video_name

        # Image dimensions: !!!!!!!!MODIFY ACCORDING TO THE VIDEO!!!!!!!!
        self.height = 480
        self.width = 640

        # Size and shape of the mask:
        #                       bl          tl          tr          br
        self.polygon = np.array([[0, 460], [240, 120], [400, 120], [640, 460]])
        # Zeros matrix the size of the image to be analysed:
        self.stencil = np.zeros((self.height, self.width))

        # Filled stencil
        cv2.fillConvexPoly(self.stencil, self.polygon, 255)

        # Store 'n_lines' previous lanes
        n_lines = 7
        self.previous_lines_left = np.zeros((n_lines, 4))
        self.previous_lines_right = np.zeros((n_lines, 4))
        self.previous_lines_horizontal = np.zeros((n_lines, 4))

        #       ONLY FOR DEBUG

    ####################################################################

    def prepare_image(self, image):
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        th1 = 1000
        th2 = 2000
        apSize = 5
        L2grad = True

        canny_image = cv2.Canny(image=img_gray1, threshold1=th1, threshold2=th2, apertureSize=apSize, L2gradient=L2grad)
        # arguments: 2=minValue, 3=maxValue (both for thresholding),
        # 4=Sobel kernel size (default 3),
        # 5=true/false - specifies the method to be used (True is better)
        return canny_image

    def make_masked(self, image):
        # for whatever reason the global stencil does not work
        _stencil = np.zeros_like(image)
        cv2.fillConvexPoly(_stencil, self.polygon, 255)
        masked = cv2.bitwise_and(image, image, mask=_stencil)
        return masked

    def get_lines(self, image):
        # Parameters:
        # output - vector that stores (Xstart, Ystart, Xend, Yend)
        # 1 - image: grayscale (binary, actually)
        # 2 - rho
        # 3 - theta
        # 4 - threshold
        # ...
        theta = 2.5
        angle = np.pi / 180
        t = 60
        minLL = 15
        maxLG = 10
        lines1 = cv2.HoughLinesP(img_canny1, theta, angle, t, np.array([]), minLineLength=minLL, maxLineGap=maxLG)
        if _lines is not None:
            return _lines
        else:
            return np.array([[[np.nan, np.nan, np.nan, np.nan]]])

    def make_points(self, line):
        # global height
        slope, intercept = line
        if abs(slope) <= 0.01:
            slope = 0.01
        y1 = (self.height - 50)
        y2 = ((y1 * 3 / 5))  # !!!!!!!!!! CHANGE ACCORDING TO THE TESTS ON THE ACTUAL VIDEO !!!!!!!!!
        x1 = ((y1 - intercept) / slope)
        x2 = ((y2 - intercept) / slope)
        return np.array([int(x1), int(y1), int(x2), int(y2)])

    def average_slope_intercept(self, input_lines):
        left_fit = []
        right_fit = []
        horizontal_fit = []
        if not np.isnan(np.sum(input_lines)):
            for line in input_lines:
                for x1, y1, x2, y2 in line:
                    if x1 == x2:
                        continue
                    if abs(y1 - y2) > 20:
                        fit = np.polyfit((int(x1), int(x2)), (int(y1), int(y2)), 1)
                        slope = fit[0]
                        intercept = fit[1]
                        if slope < 0:
                            left_fit.append((slope, intercept))
                        else:
                            right_fit.append((slope, intercept))
                    else:
                        fit = np.polyfit((int(x1), int(x2)), (int(y1), int(y2)), 1)
                        horizontal_fit.append((fit[0], fit[1]))

        _left_line = np.zeros(4, 'int32')
        _right_line = np.zeros(4, 'int32')
        _horizontal_line = np.zeros(4, 'int32')
        if left_fit:
            left_fit_average = np.average(left_fit, axis=0)
            _left_line = self.make_points(left_fit_average)
        if right_fit:
            right_fit_average = np.average(right_fit, axis=0)
            _right_line = self.make_points(right_fit_average)
        if horizontal_fit:
            horizontal_fit_average = np.average(horizontal_fit, axis=0)
            _horizontal_line = self.make_points(horizontal_fit_average)

        lines_result = np.array([[_left_line, _right_line, _horizontal_line]])
        return lines_result

    def get_middle_line(self, left, right):
        # global width

        xl1 = left[0, 0]
        yl1 = left[0, 1]
        xl2 = left[0, 2]
        yl2 = left[0, 3]

        xr1 = right[0, 0]
        yr1 = right[0, 1]
        xr2 = right[0, 2]
        yr2 = right[0, 3]

        x1_avg = ((xl1 + xr1) / 2)
        x2_avg = ((xl2 + xr2) / 2)
        y1_avg = ((yl1 + yr1) / 2)
        y2_avg = ((yl2 + yr2) / 2)

        y_top = 200
        y_bot = 450
        x_bot = self.width / 2

        if abs(x1_avg - x2_avg) < 5:
            x_top = self.width / 2
            m = 0
        else:
            m = (y1_avg - y2_avg) / (x1_avg - x2_avg)
            x_top = x_bot - ((y_bot - y_top) / m)
        print(m)
        print(x1_avg, x2_avg, y1_avg, y2_avg)
        return m, np.array([x_bot, y_bot, x_top, y_top])

    def display_lines(self, image, _all_lines):
        image_aux = np.zeros_like(image)
        line_count = 0
        for line in _all_lines:
            line_count += 1
            if line.all() != np.array([0, 0, 0, 0]).all():
                x1, y1, x2, y2 = line.reshape(4)
                if line_count == 1 or line_count == 2:
                    color = (int(255), int(0), int(0))
                elif line_count == 3:
                    color = (int(0), int(255), int(0))
                elif line_count == 4:
                    color = (int(0), int(0), int(255))
                else:
                    color = (int(255), int(255), int(0))

                cv2.line(image_aux, (int(x1), int(y1)), (int(x2), int(y2)), color, 5)
        cv2.line(image_aux, (0, 200), (640, 200), (int(255), int(255), int(255)), 5)
        return cv2.addWeighted(image, 0.5, image_aux, 1, 1)

    def stack_average_lines(self, line):
        # global previous_lines_left, previous_lines_right, previous_lines_horizontal
        previous_lines_horizontal = np.vstack((self.previous_lines_horizontal, np.array([line[0, 2]])))
        previous_lines_horizontal = np.delete(previous_lines_horizontal, 0, 0)
        averaged_lines_horizontal = np.average(previous_lines_horizontal, axis=0)

        previous_lines_left = np.vstack((self.previous_lines_left, np.array([line[0, 0]])))
        previous_lines_left = np.delete(previous_lines_left, 0, 0)
        averaged_lines_left = np.average(previous_lines_left, axis=0)

        previous_lines_right = np.vstack((self.previous_lines_right, np.array([line[0, 1]])))
        previous_lines_right = np.delete(previous_lines_right, 0, 0)
        averaged_lines_right = np.average(previous_lines_right, axis=0)

        return averaged_lines_left, averaged_lines_right, averaged_lines_horizontal

    def display_frame_nr(self, counter):
        counter = counter + 1
        print(counter)
        return counter

    def operate(self, img):

        img_aux = np.copy(img)
        img_aux = self.prepare_image(img_aux)
        img_aux = self.make_masked(img_aux)

        lines = self.get_lines(img_aux)
        averaged_lines = self.average_slope_intercept(lines)

        left_line, right_line, horizontal_line = self.stack_average_lines(averaged_lines)

        slope_c, middle_line = self.get_middle_line(np.array([left_line]), np.array([right_line]))

        all_lines = np.array([averaged_lines[0, 0], averaged_lines[0, 1], middle_line])

        if all_lines is not None:
            result = self.display_lines(img, all_lines)
            return result
        else:
            return img


    '''
        'slope_c' should be used for control !!!!!!!!!!! 
    '''
