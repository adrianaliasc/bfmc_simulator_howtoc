#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/gleip/Documents/bfmc_simulator_howtoc/startup_workspace/devel:$CMAKE_PREFIX_PATH"
export PWD='/home/gleip/Documents/bfmc_simulator_howtoc/startup_workspace/build'
export ROSLISP_PACKAGE_DIRECTORIES="/home/gleip/Documents/bfmc_simulator_howtoc/startup_workspace/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/gleip/Documents/bfmc_simulator_howtoc/startup_workspace/src:$ROS_PACKAGE_PATH"